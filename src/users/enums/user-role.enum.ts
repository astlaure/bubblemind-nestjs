export enum UserRole {
  ADMIN,
  MANAGER,
  USER,
}
