import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs/promises';
import * as path from 'path';
import { Media } from './media.entity';
import { Repository } from 'typeorm';
import { MediumMetadataDto } from './models/medium-metadata.dto';

@Injectable()
export class MediaService {
  constructor(
    @InjectRepository(Media) private mediaRepository: Repository<Media>,
  ) {}

  async findAll() {
    return await this.mediaRepository.find();
  }

  async findById(id: number) {
    return await this.mediaRepository.findOne({ where: { id } });
  }

  async save(file: Express.Multer.File) {
    const media = new Media();
    media.uri = file.filename;
    media.mimetype = file.mimetype;

    await this.mediaRepository.save(media);
  }

  async update(id: number, metadata: MediumMetadataDto) {
    const medium = await this.mediaRepository.findOne({ where: { id } });

    if (!medium) {
      return;
    }

    medium.description = metadata.description;
    await this.mediaRepository.save(medium);
  }

  async delete(id: number) {
    const medium = await this.mediaRepository.findOne({ where: { id } });
    await fs.rm(path.resolve(medium.uri), { force: true });
  }
}
