import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { MediaService } from './media.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { MediumMetadataDto } from './models/medium-metadata.dto';

@Controller('/media/:slug')
export class MediaController {
  constructor(private mediaService: MediaService) {}

  @Get()
  async getMedia() {
    return await this.mediaService.findAll();
  }

  @Get('/:id')
  async getMedium(@Param('id') id: number) {
    return await this.mediaService.findById(id);
  }

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async postMedium(@UploadedFile() file: Express.Multer.File) {
    await this.mediaService.save(file);
  }

  @Put('/:id')
  async patchMediumMetadata(
    @Param('id') id: number,
    @Body() metadata: MediumMetadataDto,
  ) {
    await this.mediaService.update(id, metadata);
  }

  @Delete('/:id')
  async deleteMedium(@Param('id') id: number) {
    await this.mediaService.delete(id);
  }
}
