import { Module } from '@nestjs/common';
import { MediaService } from './media.service';
import { MediaController } from './media.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Media } from './media.entity';
import { MulterModule } from '@nestjs/platform-express';
import { MediaStorage } from './media.storage';

@Module({
  imports: [
    TypeOrmModule.forFeature([Media]),
    MulterModule.register({
      storage: MediaStorage.diskStorage,
    }),
  ],
  providers: [MediaService],
  controllers: [MediaController],
})
export class MediaModule {}
