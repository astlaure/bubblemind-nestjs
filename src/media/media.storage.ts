import * as multer from 'multer';
import { Request } from 'express';
import * as fs from 'fs/promises';
import * as path from 'path';

export class MediaStorage {
  static diskStorage = multer.diskStorage({
    destination: './upload',
    async filename(
      req: Request,
      file: Express.Multer.File,
      callback: (error: Error | null, filename: string) => void,
    ) {
      if (!req.params['slug']) {
        callback(new Error('bad slug'), '');
      }

      try {
        await fs.access(path.resolve(`upload/${req.params['slug']}`));
      } catch (err) {
        console.log('creating directory');
        await fs.mkdir(path.resolve(`upload/${req.params['slug']}`));
      }

      // TODO add prefix for duplicates ??

      callback(null, `/${req.params['slug']}/${file.originalname}`);
    },
  });
}
