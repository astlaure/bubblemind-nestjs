# Media Module

## Features

1. get the media entities
2. get one media by id
3. upload a media (only the file)
4. update the media metadata (description) (not the media itself)
5. delete the media and saved file

## Endpoints

1. get all media with pagination
2. upload a media
